  // Callback for the PayFrame
  var payFrameCallback = function (error) {
    if (error) {
      // Frame could not be loaded, check error object for reason.
      console.log(error.apierror, error.message);
    } else {
      // Frame was loaded successfully and is ready to be used.
      console.log("PayFrame successfully loaded");
      $("#payment-form").show(300);
    }
  };


$(document).ready(function () {
  paymill.embedFrame('credit-card-fields', {
    lang: 'en'
  }, payFrameCallback);
});

var amount2payincents = 0;

Shiny.addCustomMessageHandler("amountvar", createAmountVar);

function createAmountVar(amountvar) {
  console.log("We got amountvar from R: " + amountvar);
  amount2payincents = amountvar;
}

var submitForm = function () {
  
  console.log("amount2payincents = " + amount2payincents);
  paymill.createTokenViaFrame({
    amount_int: amount2payincents,
    currency: 'EUR'
  }, function(error, result) {
    // Handle error or process result.
    if (error) {
      // Token could not be created, check error object for reason.
      console.log(error.apierror, error.message);
    } else {
      // Token was created successfully and can be sent to backend.
      Shiny.onInputChange("paymill_token", result.token);
    }
  });

  return false;
}
