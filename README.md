## Intro

paymiller is working but not tested in wild life R package to process credit cards payments in Shiny apps through online payment provider [PAYMILL](https://www.paymill.com/en/). It is expected to work with EU-based companies and to accept payments worldwide.

## Warning

If you want to try to accept payments through PAYMILL don't repeat my mistake: don't invest too much time in it before successful account activation, as PAYMILL can refuse to work with you without giving any reason (see quoted PAYMILL answer at the bottom). 

## Installation

```r
remotes::install_git("https://gitlab.com/rresults/paymiller.git")
```

## Set up

### Public and private keys

In order to use PAYMILL you must register an account. Before activation you will be provided with [test keys pair](https://developers.paymill.com/guides/introduction/your-account). After activation you are to replace it with live keys pair. 

Package paymiller looks for the keys in two places. If the environment is inside of a docker (`\.dockerenv` file exists), then keys are taken from [docker's secrets](https://docs.docker.com/engine/swarm/secrets/) `/run/secrets/paymill_private` and `/run/secrets/paymill_public`. Without docker the keys are expected to be found in `PAYMILL_PRIVATE` and `PAYMILL_PUBLIC` environmental variables.

### PAYMILL JavaScript bridge

To be compliant with today's strict legislation on Internet payments you not to touch/transfer/keep credit card data yourself. PAYMILL provides you with a JavaScript library [Bridge](https://developers.paymill.com/guides/reference/bridge-payframe). With the JS-library loaded when a user visits your Shiny application, the Bridge inserts a form to accept credit card data from the user and sends the data directly to PAYMILL server.

To make this work you have to add your public PAYMILL key and the JS library in UI part of your app and to specify location in the page, where you want to put the form with credit card fields. To accomplish this you can use paymiller's `creditCardFieldsUI` function.

After the user submits the payment information it is processed by PAYMILL server. In case of success you get a token from PAYMILL. It appears in `input$paymill_token` hidden input of your Shiny app/web page. Then you use the token to create payment or transaction to complete the purchase.

Highly likely in live mode PAYMILL requires currency amounts [to be equal](https://gitlab.com/rresults/paymiller/issues/1) in JS Bridge and later in API request. In test mode PAYMILL doesn't check it. In our package currently it is implemented only in a sandbox application `generate_token_app()`, but not in the main function `paymill_create_transaction()`.

### Create transaction

When you get the token (`observeEvent(input$paymill_token)`), you finalize the payment with `paymill_create_transaction()`. 

### Other functions

* `paymill_transactions` lists transactions;
* `paymill_clients` lists clients;
* `paymill_create_client` creates new client;
* `paymill_recent_address` retrieves recent address for a given client;
* `paymill_test_info` returns HTML with test credit cards data; 
* `paymill_api` is the workhorse of the package to communicate with PAYMILL API (not exported).

There is `copy_card_logos` function also. The idea was to copy card logos and security signs (locks etc) to a Shiny app at its start by means of the app itself. But issues with writing permissions arise in Shiny Server. So it is easier and safer to copy these files manually. Images are not in pure CC0 public domain, so you have to credit them. For credit info on secure indicators see [Issue 11](https://gitlab.com/rresults/paymiller/issues/11), on cards logos: [Issue 10](https://gitlab.com/rresults/paymiller/issues/10). 

To manage user data and access take a look at [backendlessr](https://gitlab.com/rresults/backendlessr/) package.

## Feedback

Please fill an [issue](https://gitlab.com/rresults/paymiller/issues/new) or send an [email](mailto:info@rresults.consulting) in case of questions or suggestions concerning the package.


## PAYMILL refusal to activate account

```
Date: Mon, 28 May 2018 09:15:33 +0200
From: neukunde paymill <neukunde@paymill.de>
To: info@rresults.consulting
Subject: Your Paymill Account

Dear Sir or Madam,

Thank you for contacting PAYMILL and your interest in working with us.

We have checked your application case carefully, but we regret to inform you
that we unfortunately cannot complete your activation process with PAYMILL.

Please understand, that we sometimes have to reject business models. This can
be due to technical reasons or your business model does not meet specific risk
and compliance guidelines of our partner banks.

We are constantly working hard on expanding our product, but sadly we cannot
meet your requirements at this point in time. Bank rules for accepting or
rejecting merchant account applications are numerous and complex.

We appreciate your understanding, that we cannot disclose specific details on
the evaluation process. Please also note, that this decision is unfortunately
final and can not be reversed.

We wish you all the best for the future.

Kind regards,

Your PAYMILL Team

PAYMILL GmbH | St.-Martin-Straße 63, 81669 München

```



